from flask import Flask, render_template, request, redirect, url_for, send_from_directory, send_file, flash
from jinja2 import Environment, FileSystemLoader
import requests as r
import json
from flask_cors import CORS
import os
import random
APP = Flask(__name__)
import string
CORS(APP)
import random
import gunicorn
APP.secret_key = ''.join(random.choices(string.ascii_uppercase + string.digits, k=32))


def remove_duplicate_edges(nodes_and_edges):
    unique_edges = set()
    processed_list = []
    for objs in nodes_and_edges:
        objs = objs['p1']
        if len(objs) > 1:
            start_node = objs[0]['name']
            end_node = objs[2]['name']
            label = objs[1]
            edge = (start_node, end_node, label)
            edge_2 = (end_node, start_node, label)
            if edge not in unique_edges and edge_2 not in unique_edges:
                unique_edges.add(edge)
                processed_list.append(objs)
    return processed_list


def get_nodes_and_edges(nodes_and_edges):
    nodes = []
    edges = []
    known_node_names = []
    num = 1
    unique_edges = set()
    nodes_and_edges = remove_duplicate_edges(nodes_and_edges)

    for objs in nodes_and_edges:
        # objs = objs['p1']
        if len(objs) == 1:
            nodes.append({
                'name': objs[0]['name'],
                'id': num,
                'posX': random.randint(-400, 400),
                'posY': random.randint(-400, 400)
            })
        else:
            start_node = objs[0]['name']
            end_node = objs[2]['name']

            if start_node not in known_node_names:
                nodes.append({
                    'name': start_node,
                    'id': num,
                    'posX': random.randint(-400, 400),
                    'posY': random.randint(-400, 400)
                })
                known_node_names.append(start_node)
                start_node_id = num
                num += 1
            else:
                start_node_id = next(node['id'] for node in nodes if node['name'] == start_node)

            if end_node not in known_node_names:
                nodes.append({
                    'name': end_node,
                    'id': num,
                    'posX': random.randint(-400, 400),
                    'posY': random.randint(-400, 400)
                })
                known_node_names.append(end_node)
                end_node_id = num
                num += 1
            else:
                end_node_id = next(node['id'] for node in nodes if node['name'] == end_node)

            label = objs[1]
            edge = (start_node_id, end_node_id, label)

            if edge not in unique_edges:
                unique_edges.add(edge)
                edges.append({
                    'id': num,
                    'start': start_node_id,
                    'end': end_node_id,
                    'label': label
                })
                num += 1

    return nodes, edges




@APP.route("/", methods=["GET", "POST"])
def index():
    """ Dashboard """
    post_data_nodes = {"query": "MATCH (a) RETURN count(a) as b"}
    number_of_nodes = r.post("http://0.0.0.0:9000/graphing/query", json=post_data_nodes).json()[0]['b']
    post_data_edges = {"query": "MATCH (a)-[b]-(c) RETURN count(b) as e"}
    number_of_edges = r.post("http://0.0.0.0:9000/graphing/query", json=post_data_edges).json()[0]['e']
    pwned = r.post("http://0.0.0.0:9000/graphing/query", json={"query": "MATCH (a) WHERE a.pwned = 'True' RETURN count(a) as b"}).json()[0]['b']
    pwned_edges = r.post("http://0.0.0.0:9000/graphing/query", json={"query": "MATCH (a)-[b]->(c) WHERE a.pwned = 'True' and not type(b) = 'memberOf' and not type(b) = 'contains' RETURN count(b) as d"}).json()[0]['d']
    all_nodes = r.post("http://0.0.0.0:9000/graphing/query", json={"query": "MATCH (a) RETURN a"}).json()
    formatted_all_nodes = []
    for node in all_nodes:
        node = node['a']
        formatted_all_nodes.append(node)
    if request.method == "POST":
        return redirect('/graph')
    query = """MATCH p1=(a) where (a.t = 'computer') return p1"""
    nodes_and_edges = r.post("http://0.0.0.0:9000/graphing/query", json={"query":  query}).json()
    nodes, edges = get_nodes_and_edges(nodes_and_edges)
    return render_template('dashboard.html', number_of_nodes=number_of_nodes, number_of_edges=number_of_edges, pwned=pwned, pwned_edges=pwned_edges, nodes=nodes, edges=edges, all_nodes=formatted_all_nodes)






@APP.route('/graph', methods=['GET', 'POST'])

def graph():
    if request.method == 'GET':
        nodes_and_edges = r.post("http://0.0.0.0:9000/graphing/query", json={"query": """MATCH p1=(c)-[b]->(a) where (a.pwned = 'True') return p1 LIMIT 30""" }).json()
        nodes, edges = get_nodes_and_edges(nodes_and_edges)

    if request.method == 'POST':
        q = request.form['query']
        if ">" not in q and "<" not in q:
            flash("Invalid query format. Please use specify the direction of the relationship using '>' or '<'.")
            return redirect('/graph')
        # flash("")
        q =  f"""MATCH p1={str(q)} RETURN p1"""
        nodes_and_edges = r.post("http://127.0.0.1:9000/graphing/query", json={"query": q})
        nodes_and_edges = nodes_and_edges.json()
        nodes, edges = get_nodes_and_edges(nodes_and_edges)

    return render_template('graph.html', nodes=nodes, edges=edges)



@APP.route("/get_node", methods=["POST"])
def get_node():
    node_name = request.json.get("node_to_get")
    nodes_and_edges = r.post("http://0.0.0.0:9000/graphing/query", json={"query": f"MATCH (n) WHERE n.name = '{node_name}' RETURN n"}).json()
    node_data = nodes_and_edges[0]["n"]
    return node_data





@APP.route("/get_edge", methods=["POST"])
def get_edge():
    start_node = request.json.get("start_node")
    end_node = request.json.get("end_node")
    edge_type = request.json.get("edge_type")
    nodes_and_edges = r.post("http://0.0.0.0:9000/graphing/query", json={"query": f"MATCH (n)-[r:{edge_type}]-(m) WHERE n.name = '{start_node}' AND m.name = '{end_node}' RETURN properties(r) as prop"}).json()
    print(nodes_and_edges)
    edge_data = nodes_and_edges[0]["prop"]
    return edge_data



if __name__ == "__main__":
    
    APP.run(debug=False, host='0.0.0.0', port=5000)

