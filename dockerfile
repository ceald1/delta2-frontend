# Use the official Python image as the base image
FROM python:3.9-slim

# Set environment variables
ENV FLASK_APP=app.py
ENV FLASK_RUN_HOST=0.0.0.0
ENV FLASK_RUN_PORT=5000

# Set the working directory in the container
WORKDIR /app

# Copy the requirements file and install Python dependencies
COPY requirements.txt requirements.txt
RUN pip install --no-cache-dir -r requirements.txt

# Install Node.js and npm
RUN apt-get update && apt-get install -y nodejs npm

# Copy the Flask app files to the working directory
COPY . .

# Install JavaScript dependencies using npm
RUN cd static && npm install @memgraph/orb

# Expose the Flask app port
EXPOSE 5000

# Run the Flask app
CMD ["gunicorn", "--config=gunicorn_config.py", "-w 10", "app:APP"]
