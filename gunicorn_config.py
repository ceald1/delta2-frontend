import os

forwarded_allow_ips = '*'

secure_scheme_headers = { 'X-Forwarded-Proto': 'https' }

bind = os.environ.get('GUNICORN_BIND', '0.0.0.0:5000')